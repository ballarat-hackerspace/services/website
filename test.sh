#!/bin/bash

# require cover (via Devel::Cover) and prove to be installed
MOJO_LOG_LEVEL=fatal cover -t -make 'prove -Ilib -r t -l -v; echo >/dev/null'
