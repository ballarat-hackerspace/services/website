% layout 'page', active_page => 'sponsors', page_title => 'Sponsors';

<!-- sponsors section -->
<section id="sponsors" class="section">
  <div class="container">
    <h2 class="title text-center">Corporate Sponsors</h2>
    <div class="row">
      <div class="col-sm-4 text-center">
        <a href="http://www.federation.edu.au/"><img class="spnsr-logo" src="/images/spnsrs/logo-feduni-light-background.png"></img></a>
      </div>
      <div class="col-sm-4 text-center">
        <a href="https://www.eurekative.com.au/"><img class="spnsr-logo" src="/images/spnsrs/logo-eurekative.png"></img></a>
      </div>
      <div class="col-sm-4 text-center">
        <a href="http://www.lateralplains.com"><img class="spnsr-logo" src="/images/spnsrs/logo-lateral-plains.png"></img></a>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4 text-center">
        <a href="http://www.sedadvisory.com.au/"><img class="spnsr-logo" src="/images/spnsrs/logo-sed-light-background.png"></img></a>
      </div>
      <div class="col-sm-4 text-center">
        <a href="http://www.datapipeline.com.au/"><img class="spnsr-logo" src="/images/spnsrs/logo-datapipeline.png"></img></a>
      </div>
      <div class="col-sm-4 text-center">
        <a href="http://www.ballarat.vic.gov.au/"><img class="spnsr-logo" src="/images/spnsrs/logo-city-of-ballarat-light-background.png"></img></a>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4 text-center">
        <a href="https://lab79.io/"><img class="spnsr-logo" src="/images/spnsrs/logo-lab_79.svg"></img></a>
      </div>
    </div>
  </div><!-- container -->
</section><!-- sponsors section -->

<section id="seeders" class="section">
  <div class="container">
    <h2 class="title text-center">Seeders</h2>
    <div class="row">
      <div class="col-sm-12 text-center">
        <table class="table table-hover table-seeders">
          <tr>
            <td>Scott Weston</td>
            <td><span class="trophy"><i class="fa fa-trophy"></i><i class="fa fa-trophy"></i><i class="fa fa-trophy"></i><i class="fa fa-trophy"></i><i class="fa fa-trophy"></i></span></td>
          </tr>
          <tr>
            <td>Ian Firns</td>
            <td><span class="trophy"><i class="fa fa-trophy"></i><i class="fa fa-trophy"></i><i class="fa fa-trophy"></i><i class="fa fa-trophy"></i></span></td>
          </tr>
          <tr>
            <td>Brett James</td>
            <td><span class="trophy"><i class="fa fa-trophy"></i><i class="fa fa-trophy"></i><i class="fa fa-trophy"></i><i class="fa fa-trophy"></i></span></td>
          </tr>
          <tr>
            <td>Stuart Clark</td>
            <td><span class="trophy"><i class="fa fa-trophy"></i></span></td>
          </tr>
        </table>
      </div>
    </div>
  </div><!--//container-->
</section><!-- photos section-->

<!-- sponsors section -->
<section id="sponsors" class="section">
  <div class="container">
    <h2 class="title text-center">Why you should sponsor us</h2>
    <div class="row">
      <p>Since our inception in 2014, the Ballarat Hackerspace has been a nexus for Ballarat's technology innovators, maker community and creatives.</p>
      <p>A "hacker" is a person who uses their technical knowledge to achieve a goal or overcome an obstacle. We void warranties, not break laws. We do so to learn about technology, teach others, and develop the next generation of tech.</p>
      <p>The Ballarat Hackerspace is entirely volunteer-run and a non-profit organisation. Over the last eight years of operation, we have provided countless hours of assistance for members and the wider community.
      Those impacted by our work have upskilled in technology, electronics, programming, science, maths, art and many other areas.
      <p>Our work has assisted many past and current members in finding employment in these in-demand fields and provided an exciting and productive hobby. In addition, many current and former members further share that knowledge, providing a multiplier effect on the impact of our work.</p>
      <p>We have had people learn these skills with us from 4yo to 96yo from a variety of backgrounds.</p>
      <p>Sponsors support our mission of growing the skills of the broader community in these regions. Our overheads are extremely low, so nearly all sponsored funds go directly to fulfilling our mission.</p>
      <p>Sponsors are welcome at any level, and we'd love for you to get in touch regarding how to complete your sponsorship.</p>
      <p>For more information, please email us at committee@ballarathackerspace.org.au or drop in for a visit to see the space and learn more.</p>
    </div>
  </div><!-- container -->
</section><!-- sponsors section -->