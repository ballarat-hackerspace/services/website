---
author: Committee
feature: /images/blog/virtualhackerspace.png
title: Virtual Hackerspace Hours
tags: covid-19, coronavirus
---

G'day hackers,

We've just had confirmation from the Victorian government that the state of
emergency has been extended for a month until at least May 11. To get us
through this period we have decided to run some virtual hackerspace events on
the [Jitsi video conferencing system](https://jitsi.org). We will initially be
running virtual get togethers during the following hours:

* Mondays: 7pm to 8pm
* Tuesdays: 4pm to 5pm
* Wednesdays: 11am to 12pm
* Saturdays: 2pm to 3pm

These hours may change over time so keep an eye on Slack for any last minute
changes. You will require the Chrome browser and either a microphone or a
webcam to participate and then browse to
[https://meet.jit.si/ballarathackerspace](https://meet.jit.si/ballarathackerspace).
You can also use an Apple or Android device but will require the native app.
Browsing to the URL on the device will redirect you to download the app and
once running you may be required to enter the *room name* of
`ballarathackerspace`. Note: Firefox support is coming very soon, it sort of works
now but we've found the video eventually freezes up after a few minutes.

The idea is you can connect to the room to ask any question or show off any
projects you've been working on during this isolation period. Or you can join
simply to break up the monotony of our current situation and have a bit of a
chat.

Feel free to connect to and use the [virtual
room](https://meet.jit.si/ballarathackerspace) at any time (not just the posted
times above) and you can try summoning members via Slack if it gets too
lonely/quiet and you need some company.

Take care of yourself and your loved ones, and we're all looking forward to
the time we can re-open the hackerspace.

Committee.
