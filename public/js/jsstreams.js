// // fetch(`https://ballarathackerspace.org.au/meta/streams.json`)
// fetch(`streams.json`)
//   .then(response => response.json() )
//   .then(data => streamsApi(data) );

let lastFetch = 0;
let delayLength = 500;
let retries = 1;

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

function loadMethod(type = "websocket") {
  if (type == "websocket") {
    url = "wss://ballarathackerspace.org.au:/meta/streams-ws";
    ws = new WebSocket(url);
    ws.onclose = function() {
        document.querySelector(".loading-dot").classList.add("animated");
        delayLength = delayLength + (250 * retries )
        console.info(`Connection closed, trying to reopen after waiting ${delayLength}`);
        setTimeout(function() {
            loadMethod((type = "websocket"));
        }, delayLength);
        delete ws;
        retries++;
    };
    ws.onopen = function() {
        document.querySelector(".loading-dot").classList.remove("animated");
      console.log("Connection opened.");
      delayLength = 500;
      retries = 1;
    };
    ws.onmessage = function(msg) {
      var res = JSON.parse(msg.data);

      if (Object.keys(res).length > 0) {
        streamsApi(res, (websocket = true));
      }
    };
  } else {
    // fetch("streams.json")
    sensorTimer = setInterval(function() {
      fetch("/meta/streams.json")
        .then(handleErrors)
        .then(response => response.json())
        .then(data => streamsApi(data))
        .catch(error => console.log(error));
    }, 30000);
  }
}

loadMethod("websocket");

function hide() {
  let element = document.getElementById("streams");
  element.style["opacity"] = 0;
}

function show() {
  let element = document.getElementById("streams");
  element.style["opacity"] = 1;
}

const streamsApi = (data, websocket = true) => {
  let fetchTime = websocket ? data.timestamp : data.ts;

  let fetchDate = new Date(parseInt(fetchTime) * 1000);
  fetchHeading = `${fetchDate.getDate()} ${fetchDate.getMonth()}`;
  // console.log('Getting data for', fetchHeading);
  let gridStart = '<ul class="list-group">';
  let gridItems = "";
  let freshData = "";
  let gridEnd = "</ul>";
  console.info("We're about to check if we should run", fetchTime, lastFetch);
  if (fetchTime >= lastFetch) {
    console.info("... and we should.");

    allLocalStreams = document.querySelector("#streams");
    let thisRun = [];

    const parseStream = item => {
      // console.log(item)
      let itemDate = new Date(parseInt(item.timestamp) * 1000);
      let points = item.points;
      let data = item.data;
      // dateString = theDate.toGMTString();
      // console.log( itemDate.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'} ) );
      let streamName = item.stream;
      let timestamp = item.timestamp;
      // console.log("array", thisRun);

      // https://ballarathackerspace.org.au/meta/streams/monitor/door/latch
      // Call a function instead
      // Does it exist?
      exists =
        document.querySelectorAll(`#streams [data-key="${streamName}"]`)
          .length > 0;
      // Strip away things that are in the list more than once using thisRun...
      // Todo create a virtual dom so I can use the one call and include it in  updates instead
      if (!exists && !thisRun.includes(streamName)) {
        formatted = `<li class="list-group-item" data-key="${streamName}" data-timestamp="${timestamp}">
                <a href="https://ballarathackerspace.org.au/meta/streams/${streamName}">${streamName}</a>
                - <div class="datapoints">
                    <span data-datapoint="true">${data}</span>
                </div> <br>
                <span class="time">${itemDate.toLocaleTimeString([], {
                  hour: "2-digit",
                  minute: "2-digit"
                })}
                </span></li>\n`;
        freshData = freshData + formatted;
      } else {
        // console.log("we are doing an update")
        // Don't do anything if we've already done a datapoint for this item. Don't need this if we had a virtual dom used.
        if (thisRun.includes(streamName)) {
          console.log(`Skipping ${streamName}`);
          return;
        }
        // Do an update
        let previousItem = document.querySelector(
          `#streams [data-key="${streamName}"]`
        );
        let oldTimestamp = previousItem.dataset.timestamp;
        // console.log(oldTimestamp, timestamp, previousItem);
        if (timestamp > oldTimestamp) {
          // Set a new time
          previousItem.querySelector(
            ".time"
          ).innerHTML = itemDate.toLocaleTimeString([], {
            hour: "2-digit",
            minute: "2-digit"
          });
          // Format data points
          previousItem
            .querySelectorAll(".datapoints span")
            .forEach((e, index) => {
              if (index >= 4) {
                // Get rid of too many matches
                e.parentNode.removeChild(e);
              }
              // console.log("wo",index, e)
            });

          // datapoints = previousItem.querySelector(".datapoints").prepend("<p>I am a new thing<p>");
          // Add the newest one
          datapoints = previousItem
            .querySelector(".datapoints")
            .insertAdjacentHTML(
              "afterbegin",
              `<span data-datapoint="true">${data}</span>`
            );
          // console.log("data", datapoints);
          // TODO: .map sort the list by timestamp
          existingList = allLocalStreams.querySelector("ul");
          // Shift to the top.
          existingList.prepend(previousItem);
        }
      }
      gridItems = gridItems + formatted;
      // console.log(streamName, gridItems)
      thisRun.push(streamName);
    };

    if (websocket) {
      parseStream(data);
    } else {
      data.streams.items.forEach(item => {
        parseStream(item);
      });
    }

    mqttArea = document.querySelector("#streams");
    if (mqttArea.querySelectorAll("li.list-group-item").length == 0) {
      mqttArea.innerHTML = `${gridStart}${gridItems}${gridEnd}`;
      // Got data? Lets sort it. Still experimental.
      // items = Array.prototype.slice.call(document.querySelectorAll('ul.list-group .list-group-item'));
      //     items.sort(function(a,b) {
      //     return a.dataset.timestamp < b.dataset.timestamp;
      // });

      // items.forEach(function(item, idx)
      // {
      //     if(idx>0)
      //     item.parentNode.insertBefore(item, items[idx-1]);
      // });
    } else {
      mqttArea = document.querySelector("#streams");
      // We should add new data that didn't already exist at all.
      // console.log("Fresh Data check: ", freshData, gridItems)
      mqttArea.firstChild.insertAdjacentHTML("afterbegin", freshData);

      // Experimental Sorting by date. Works but repaints the transition for existing data.
      // items = Array.prototype.slice.call(document.querySelectorAll('ul.list-group .list-group-item'));
      // items.sort(function(a,b) {
      //     return a.dataset.timestamp < b.dataset.timestamp;
      // });

      // items.forEach(function(item, idx)
      // {
      //     if(idx>0)
      //     item.parentNode.insertBefore(item, items[idx-1]);
      // });
    }
  }
  // Update the runtime for next interval
  lastFetch = fetchTime;
};
