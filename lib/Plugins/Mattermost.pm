#
# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

package Plugins::Mattermost;
use Mojo::Base 'Mojolicious::Plugin';

use Mojo::UserAgent;
use Mojo::Util qw(dumper);

my $ua = Mojo::UserAgent->new;

our $VERSION = '0.1';

has 'url';
has 'channels';

sub register {
  my ($self, $app, $config) = @_;

  $self->url($config->{url} // '');
  $self->channels($config->{channels} // {});

  my $channels = keys %{$self->channels};

  $app->log->info(sprintf("Mattermost endpoint at: %s (channels: %d)", $self->url, $channels));

  $app->helper('mattermost.post' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    return unless $args->{text};

    my $data = {
      text => $args->{text}
    };

    $data->{username} = $args->{username} if $args->{username};
    $data->{icon} = $args->{icon} if $args->{icon};
    $data->{icon_emoji} = $args->{emoji} if $args->{emoji};

    if (my $channel = $args->{channel}) {
      $data->{channel} = $self->channels->{$channel} // $channel;
    }

    return $ua->post_p($self->url => json => $data);
  });
}

1;
