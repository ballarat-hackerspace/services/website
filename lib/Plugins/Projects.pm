#
# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

package Plugins::Projects;
use Mojo::Base 'Mojolicious::Plugin';

#
# This plugin requires plugins ::Members
#

use Mojo::JSON qw(decode_json encode_json);
use Mojo::SQLite;
use Mojo::Util qw(dumper trim);

use Carp 'croak';
use DBI;
use Time::Piece;

our $VERSION = '0.1';

has 'sqlite';

sub register {
  my ($self, $app, $config) = @_;

  $config->{db} //= ':temp:';

  $self->sqlite(state $sqlite = Mojo::SQLite->new->from_filename($config->{db}));

  $app->log->info("Using projects database: " . $config->{db});

  # conditionally build table
  $self->sqlite->db->query("
    CREATE TABLE IF NOT EXISTS projects (
      id          INTEGER PRIMARY KEY AUTOINCREMENT,
      email       CHAR(128) NOT NULL DEFAULT '',
      stub        CHAR(256) NOT NULL UNIQUE,

      title       CHAR(256) NOT NULL,

      description TEXT,

      image_url   CHAR(256),

      meta        TEXT NOT NULL DEFAULT '{}',

      created     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      updated     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    )
  ");

  $self->sqlite->db->query("
    CREATE TABLE IF NOT EXISTS projects_logs (
      id        INTEGER PRIMARY KEY AUTOINCREMENT,
      project   INTEGER NOT NULL,

      sequence  INTEGER NOT NULL DEFAULT 0,
      data      TEXT NOT NULL DEFAULT '{}',

      created   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      updated   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    )
  ");

  $app->helper('projects.add' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $meta = encode_json($args->{meta} // {});

    my $stub = _sanitise_stub($args->{title});

    my $logs = $args->{logs} // [];

    eval {
      my $db = $self->sqlite->db;
      my $tx = $db->begin;
      my $project_id = $db->query(
        'INSERT INTO projects (email, stub, title, description, image_url, meta) VALUES (?, ?, ?, ?, ?, ?)',
        $args->{email}, $stub, $args->{title}, $args->{description},
        $args->{image_url}, $meta
      )->last_insert_id;

      my $sequence = 0;
      for my $log (@{$logs}) {
        my $data = encode_json($log->{data});

        $db->query('INSERT INTO projects_logs (project, sequence, data) VALUES (?, ?, ?)', $project_id, $sequence, $data);

        $sequence++;
      }

      $tx->commit;
    };

    if ($@) {
      $app->log->error("Unable to add project: $@");
      return !!0;
    }

    return !!1;
  });

  $app->helper('projects.find' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $fetch = $args->{fetch} // 1000;
    my $filter = [];
    my $filter_arg = [];

    if ($args->{stub}) {
      push @{$filter}, 'stub=?';
      push @{$filter_arg}, $args->{stub};
    }

    if ($args->{title}) {
      push @{$filter}, 'title=?';
      push @{$filter_arg}, $args->{title};
    }

    if ($args->{email}) {
      push @{$filter}, 'email=?';
      push @{$filter_arg}, $args->{email};
    }

    if ($args->{relative}) {
      my ($r_ts, $r_id) = split /\./, $args->{relative};
      push @{$filter}, '(created_ts, id) > (?, ?)';
      push @{$filter_arg}, $r_ts+0, $r_id+0;
    }

    my $where = @{$filter} ? ' WHERE ' . join(' AND ', @{$filter}) : '';
    my $limit = " ORDER BY created, id LIMIT $fetch";

    #
    # note: we can't alias a virtual column as an already existing one
    # e.g STRFTIME('%s', created) AS created
    #
    my $sql = "SELECT id, email, stub, title, description, image_url, meta, STRFTIME('%s', created) AS created_ts, STRFTIME('%s', updated) AS updated_ts FROM projects $where $limit";

    my $projects = $self->sqlite->db->query($sql, @{$filter_arg})->hashes->each(sub {
      my $d = shift;

      $d->{created} = delete $d->{created_ts};
      $d->{updated} = delete $d->{updated_ts};

      $d->{meta} = decode_json $d->{meta};
      $d->{meta}{tags} = [grep {length} @{$d->{meta}{tags}}];
    });

    my $data = {
      items => $projects->to_array,
    };

    my $last = $projects->last;
    $data->{relative} = "$last->{created}.$last->{id}" if $last;

    return $data;
  });

  $app->helper('projects.delete' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $filter = [];
    my $filter_arg = [];

    if ($args->{id}) {
      push @{$filter}, 'id=?';
      push @{$filter_arg}, $args->{id};
    }

    if ($args->{stub}) {
      push @{$filter}, 'stub=?';
      push @{$filter_arg}, $args->{stub};
    }

    if ($args->{title}) {
      push @{$filter}, 'title=?';
      push @{$filter_arg}, $args->{title};
    }

    if ($args->{email}) {
      push @{$filter}, 'email=?';
      push @{$filter_arg}, $args->{email};
    }

    return undef unless @{$filter};

    my $where = ' WHERE ' . join(' AND ', @{$filter});

    return !!$self->sqlite->db->query("DELETE FROM projects $where", @{$filter_arg});
  });

  $app->helper('projects.logs' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $stub = $args->{stub} // '';

    return undef unless !!$stub;

    my $sql = "SELECT pl.id, pl.sequence, pl.data, STRFTIME('%s', pl.created) AS created_ts, STRFTIME('%s', pl.updated) AS updated_ts FROM projects_logs pl JOIN projects p ON (p.id=pl.project) WHERE p.stub=? ORDER BY sequence";

    my $logs = $self->sqlite->db->query($sql, $stub)->hashes->each(sub {
      my $l = shift;

      $l->{created} = delete $l->{created_ts};
      $l->{updated} = delete $l->{updated_ts};

      $l->{data} = decode_json $l->{data};
    });

    return $logs->to_array,
  });

  $app->helper('projects.update' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $stub = $args->{stub} // '';
    my $title = trim ($args->{title} // '');

    return undef unless !!$stub && !!$title;

    my $image_url = trim($args->{image_url} // '');
    my $description = trim($args->{description} // '');

    my $meta = encode_json($args->{meta} // {});

    my $logs = $args->{logs} // [];

    my $project_id = $self->sqlite->db->query('SELECT id FROM projects WHERE stub=?', $stub)->hashes->[0]->{id};

    # get existing logs for comparison
    my $logs_existing = $c->projects->logs(stub => $stub);

    eval {
      my $db = $self->sqlite->db;
      my $tx = $db->begin;
      $db->query('UPDATE projects SET title=?, image_url=?, description=?, meta=?, updated=CURRENT_TIMESTAMP WHERE stub=?',
        $title, $image_url, $description, $meta, $stub
      );

      my $sequence = 0;

      # process supplied log entries
      for my $log (@{$logs}) {
        my $id = $log->{id};
        my $data = encode_json($log->{data});

        # insert entries without an id
        if (!!$id) {
          my ($log_existing) = grep {$_->{id} eq $id} @{$logs_existing};

          if (
            $log_existing &&
            $log_existing->{sequence} != $log->{sequence} ||
            $log_existing->{data} != $log->{data}
          ) {
            $db->query('UPDATE projects_logs SET sequence=?, data=?, updated=CURRENT_TIMESTAMP WHERE id=?', $sequence, $data, $id);
          }
        }
        # check for update on existing ids
        else {
          $db->query('INSERT INTO projects_logs (project, sequence, data) VALUES (?, ?, ?)', $project_id, $sequence, $data);
        }

        $sequence++;
      }

      # process original log entries
      for my $log_existing (@{$logs_existing}) {
        my ($log) = grep {$_->{id} && $_->{id} eq $log_existing->{id}} @{$logs};

        # delete if no longer supplied
        unless (!!$log) {
          $db->query('DELETE FROM projects_logs WHERE id=?', $log_existing->{id});
        }
      }

      $tx->commit;
    };

    if ($@) {
      $app->log->error("Unable to update project: $@");
      return !!0;
    }

    return !!1;
  });
}

sub _sanitise_stub {
  my $stub = shift;

  $stub = lc trim $stub;

  # kill entities
  $stub =~ s/&.+?;//g;
  $stub =~ s/\./-/g;
  $stub =~ s/[^%a-z0-9 _-]//g;
  $stub =~ s/\s+/-/g;
  $stub =~ s/-+/-/g;
  $stub =~ s/-+$//g;

  return $stub;
}

1;
