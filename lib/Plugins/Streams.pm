#
# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

package Plugins::Streams;
use Mojo::Base 'Mojolicious::Plugin';

use Mojo::JSON qw(decode_json encode_json);
use Mojo::MQTT::Message;
use Mojo::SQLite;
use Mojo::Util 'dumper';

use Carp 'croak';
use List::Util 'min';
use Time::Piece;

our $VERSION = '0.1';
our $DEFAULT_LIFETIME = 31 * 24 * 60 * 60; # 31 days

has 'sqlite';
has 'tx';

my $ua = Mojo::UserAgent->new;

my $keepAliveTimer;
my $connected = 0;
my $subscribed = 0;
my $rateLimiter = {};

$ua->inactivity_timeout(0);

sub register {
  my ($self, $app, $config) = @_;

  $config->{db} //= ':temp:';

  $self->sqlite(state $sqlite = Mojo::SQLite->new->from_filename($config->{db}));

  $config->{mqtt_topic_prefix} //= 'streams/';
  $config->{mqtt_ws_url}       //= 'wss://mqtt.ballarathackerspace.org.au/mqtt';
  $config->{mqtt_ws_proto}     //= 'mqttv3.1';

  $app->log->info("Using streams database: " . $config->{db});
  $app->log->info("Streams monitoring available: " . $config->{mqtt_ws_url} . " (" . $config->{mqtt_ws_proto} . ")");

  # conditionally build table
  $self->sqlite->db->query("
    CREATE TABLE IF NOT EXISTS streams (
      id       INTEGER PRIMARY KEY AUTOINCREMENT,
      stream   CHAR(256) NOT NULL DEFAULT '',
      origin   CHAR(32) NOT NULL DEFAULT '',
      type     CHAR(16) NOT NULL DEFAULT '',
      lifetime INTEGER NOT NULL DEFAULT $DEFAULT_LIFETIME,
      data     TEXT NOT NULL DEFAULT '{}',
      meta     TEXT NOT NULL DEFAULT '{}',
      created  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    )
  ");

  $self->sqlite->db->query("
    CREATE TABLE IF NOT EXISTS streams_acls (
      id       INTEGER PRIMARY KEY AUTOINCREMENT,
      uuid     CHAR(40) NOT NULL,
      topic    CHAR(256) NOT NULL DEFAULT '',
      access   INTEGER NOT NULL DEFAULT 1,
      created  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      updated  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

      UNIQUE(uuid, topic)
    )
  ");

  $app->helper('streams.delete' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $fetch = $args->{fetch} // 10;
    my $filter = [];
    my $filter_arg = [];

    if (exists($args->{stream})) {
      push @{$filter}, 'stream=?';
      push @{$filter_arg}, $args->{stream};
    }

    my $where = '';

    if (@{$filter}) {
      $where = ' WHERE ' . join(' AND ', @{$filter});
    }

    return !!$self->sqlite->db->query("DELETE FROM streams" . $where, @{$filter_arg});
  });

  $app->helper('streams.find' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $extra = !!$args->{summary} ? "" : ", meta";
    my $fetch = $args->{fetch} // 10;
    my $filter = [];
    my $filter_arg = [];

    if ($args->{stream}) {
      push @{$filter}, 'stream=?';
      push @{$filter_arg}, $args->{stream};
    }

    if ($args->{origin}) {
      push @{$filter}, 'origin=?';
      push @{$filter_arg}, $args->{origin};
    }

    if ($args->{type}) {
      push @{$filter}, 'type=?';
      push @{$filter_arg}, $args->{type};
    }

    if ($args->{relative}) {
      my ($r_ts, $r_id) = split /\./, $args->{relative};
      push @{$filter}, '(timestamp, id) < (?, ?)';
      push @{$filter_arg}, $r_ts+0, $r_id+0;
    }

    my $where = @{$filter} ? "WHERE " . join(' AND ', @{$filter}) : '';
    my $limit = " ORDER BY created DESC, id DESC LIMIT $fetch";
    my $sql = "SELECT id, stream, type, data, STRFTIME('%s', created) AS timestamp $extra FROM streams $where $limit";

    my $last_id;

    my $points = $self->sqlite->db->query($sql, @{$filter_arg})->hashes->each(sub {
      my $p = shift;

      # attempt a JSON decode by default
      eval {
        $p->{data} = decode_json $p->{data};
      };

      $p->{timestamp} += 0;
      $p->{meta} = decode_json $p->{meta} if $p->{meta};

      $last_id = delete $p->{id};
    });

    my $data = {
      items => $points->to_array,
    };

    my $last = $points->last;
    $data->{relative} = "$last->{timestamp}.$last_id" if $last;

    return $data;
  });

  $app->helper('streams.last_values' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $fetch = $args->{fetch} // 10;
    my $filter = [];
    my $filter_arg = [];

    my $limit = " ORDER BY created DESC, id DESC LIMIT $fetch";
    my $sql = "SELECT id, stream, type, data, meta, STRFTIME('%s', created) AS timestamp FROM streams GROUP BY stream $limit";

    my $last_id;

    my $points = $self->sqlite->db->query($sql, @{$filter_arg})->hashes->each(sub {
      my $p = shift;

      # attempt a JSON decode by default
      eval {
        $p->{data} = decode_json $p->{data};
      };

      $p->{timestamp} += 0;
      $p->{meta} = decode_json $p->{meta};

      $last_id = delete $p->{id};
    });

    return {
      items => $points->to_array,
    };
  });

  $app->helper('streams.list' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $fetch = $args->{fetch} // 10;
    my $filter = [];
    my $filter_arg = [];

    if ($args->{stream}) {
      push @{$filter}, 'stream=?';
      push @{$filter_arg}, $args->{stream};
    }

    if ($args->{origin}) {
      push @{$filter}, 'origin=?';
      push @{$filter_arg}, $args->{origin};
    }

    if ($args->{type}) {
      push @{$filter}, 'type=?';
      push @{$filter_arg}, $args->{type};
    }

    if ($args->{relative}) {
      my ($r_ts, $r_id) = split /\./, $args->{relative};
      push @{$filter}, '(timestamp, id) < (?, ?)';
      push @{$filter_arg}, $r_ts+0, $r_id+0;
    }

    my $where = @{$filter} ? "WHERE " . join(' AND ', @{$filter}) : '';
    my $limit = " ORDER BY created DESC, id DESC LIMIT $fetch";
    my $sql = "SELECT DISTINCT(stream) as stream, id, STRFTIME('%s', created) AS timestamp, COUNT(id) AS points FROM streams GROUP BY stream $where $limit";

    my $last_id;
    my $streams = $self->sqlite->db->query($sql, @{$filter_arg})->hashes->each(sub {
      my $s = shift;;

      $s->{timestamp} += 0;
      $last_id = delete $s->{id};
    });

    my $data = {
      items => $streams->to_array,
    };

    my $last = $streams->last;
    $data->{relative} = "$last->{timestamp}.$last_id" if $last;

    return $data;
  });

  $app->helper('streams.publish' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $stream = $args->{stream} // '';

    # valid stream names are '' and [0-9a-zA-Z]{1,16}
    unless ($stream =~ m/[0-9a-zA-Z]{1,32}/) {
      return undef;
    }

    my $data = $args->{data};

    my $origin = $args->{origin} // '';
    my $type = $args->{type} // (ref $data eq 'HASH' ? 'json' : '');

    my $lifetime = min($args->{lifetime} // $DEFAULT_LIFETIME, $DEFAULT_LIFETIME);

    my $created = $args->{timestamp} // gmtime->strftime('%F %T');
    $data = encode_json($data) if $type eq 'json';
    my $meta = encode_json($args->{meta} // {});

    return !!$self->sqlite->db->query('INSERT INTO streams (stream, origin, type, lifetime, data, meta, created) VALUES (?, ?, ?, ?, ?, ?, ?)',
      $stream, $origin, $type, $lifetime, $data, $meta, $created
    );
  });

  $app->helper('streams.to_csv' => sub {
    my $c = shift;
    my $data = shift;

    my $csv = "stream,type,data,timestamp\n";

    for my $row (@{$data->{items}}) {
      $csv .= join(',', $row->{stream}, $row->{type}, encode_json($row->{data}), $row->{timestamp}) . "\n";
    }

    return $csv;
  });

  $app->helper('streams.acl.create' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $access = $args->{access}; # 1=r, 2=w, 3=rw
    my $topic  = $args->{topic};
    my $uuid   = $args->{uuid};

    return !!$self->sqlite->db->query('INSERT INTO streams_acls (uuid, topic, access) VALUES (?, ?, ?)',
      $uuid, $topic, $access
    );
  });

  $app->helper('streams.acl.delete' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $topic  = $args->{topic};
    my $uuid   = $args->{uuid};

    return !!$self->sqlite->db->query('DELETE FROM streams_acls WHERE uuid=? AND topic=?', $uuid, $topic);
  });

  $app->helper('streams.acl.list' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $fetch = $args->{fetch} // 10;
    my $filter = [];
    my $filter_arg = [];

    if ($args->{uuid}) {
      push @{$filter}, 'uuid=?';
      push @{$filter_arg}, $args->{uuid};
    }

    if ($args->{topic}) {
      push @{$filter}, 'topic=?';
      push @{$filter_arg}, $args->{topic};
    }

    if ($args->{relative}) {
      my ($r_ts, $r_id) = split /\./, $args->{relative};
      push @{$filter}, '(timestamp, id) < (?, ?)';
      push @{$filter_arg}, $r_ts+0, $r_id+0;
    }

    my $where = @{$filter} ? "WHERE " . join(' AND ', @{$filter}) : '';
    my $limit = " ORDER BY created DESC, id DESC LIMIT $fetch";
    my $sql = "SELECT id, uuid, topic, access, STRFTIME('%s', created) AS timestamp FROM streams_acls $where $limit";

    my $last_id;

    my $acls = $self->sqlite->db->query($sql, @{$filter_arg})->hashes->each(sub {
      my $a = shift;

      $a->{timestamp} += 0;

      $last_id = $a->{id}; # keep IDs
    });

    my $data = {
      items => $acls->to_array,
    };

    my $last = $acls->last;
    $data->{relative} = "$last->{timestamp}.$last_id" if $last;

    return $data;
  });

  $app->helper('streams.acl.update' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    warn dumper $args;

    my $id     = $args->{id};
    my $access = $args->{access}; # 1=r, 2=w, 3=rw
    my $topic  = $args->{topic};
    my $uuid   = $args->{uuid};

    return !!$self->sqlite->db->query('UPDATE streams_acls SET uuid=?, topic=?, access=? WHERE id=?',
      $uuid, $topic, $access, $id
    );
  });

  $app->helper('streams.acl.validate' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $access = $args->{access}; # 1=r, 2=w, 3=rw
    my $topic  = $args->{topic};
    my $uuid   = $args->{uuid};

    my $granted = !!0;

    # all topics under streams are publicly read/write
    if (substr($topic, 0, 8) eq 'streams/') {
      $granted = !!1;
    # all topics under monitor are publicly readable only
    } elsif ($access == 1 && substr($topic, 0, 8) eq 'monitor/') {
      $granted = !!1;
    # otherwise look for dedicated ACLs
    } else {
      my $acls = $self->sqlite->db->query('SELECT topic FROM streams_acls WHERE uuid=? AND access & ?', $uuid, $access)->hashes;

      $granted = $acls->grep(sub {
        return topic_matches(shift->{topic}, $topic);
      })->size > 0;
    }

    # rate limit writes only to a rate of 1 per second (per worker)
    if ($granted && $access == 2) {
      my $now = gmtime->epoch;

      if ($now - ($rateLimiter->{"$topic$uuid"} // 0) > 1) {
        $rateLimiter->{"$topic$uuid"} = $now;
      } else {
        $granted = !!0;
      }
    }

    return $granted;
  });

  $app->helper('streams.mqtt.start' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    return if $connected;

    $args->{on_connect} //= sub {};
    $args->{on_data}    //= sub {};

    $ua->websocket($config->{mqtt_ws_url} => [$config->{mqtt_ws_proto}] => sub {
      my ($ua, $tx) = @_;
      $app->log->info('MQTT: connecting ...');

      unless ($tx->is_websocket) {
        $app->log->info('MQTT: handshake failed!');
        $connected = 0;
        $subscribed = 0;

        return;
      }

      $self->tx($tx);

      $tx->on(finish => sub {
        my ($tx, $code) = @_;
        $app->log->info("MQTT: closed ($code), attempting reconnect ...");

        $connected = 0;
        $subscribed = 0;
        $self->tx(undef);

        Mojo::IOLoop->remove($keepAliveTimer) and undef $keepAliveTimer if $keepAliveTimer;
      });

      $tx->on(binary => sub {
        my ($tx, $bytes) = @_;

        my $message = Mojo::MQTT::Message->new_from_bytes($bytes);

        # CONNACK
        if ($message->{type} == 2) {
          $app->log->info('MQTT: subscribing ...');
          my $m = Mojo::MQTT::Message->new(subscribe => {topics => [$config->{mqtt_topic_prefix} . '#']});
          $tx->send({binary => $m->encode});
        }
        # PUBLISH
        elsif ($message->{type} == 3) {
          my $stream = $message->{topic};
          $stream =~ s/$config->{mqtt_topic_prefix}//;

          my $data = $message->{data};

          $args->{on_data}->(stream => $stream, data => $data, timestamp => gmtime->epoch);
        }
        # SUBACK
        elsif ($message->{type} == 9) {
          $app->log->info('MQTT: subscribed.');
          $subscribed = 1;
        }
        # PINGACK
        elsif ($message->{type} == 13) {
          $app->log->debug('MQTT: ping/pong.');
        }
        else {
          $app->log->debug('MQTT: unhandled message.', dumper $message);
        }
      });

      $app->log->info('MQTT: connected.');
      $connected = 1;

      # send connection request
      my $m = Mojo::MQTT::Message->new(connect => {client_id => 'streams-proxy-'.$$});
      $tx->send({binary => $m->encode});

      $keepAliveTimer = Mojo::IOLoop->recurring(60 => sub {
        my $m = Mojo::MQTT::Message->new(pingreq => {});
        $tx->send({binary => $m->encode});
      });
    });
  });

  $app->helper('streams.mqtt.publish' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    if ($self->tx) {
      my $m = Mojo::MQTT::Message->new(publish => {
        topic => $config->{mqtt_topic_prefix} . $args->{stream},
        data => $args->{data}
      });

      $self->tx->send({binary => $m->encode});
    }
  });
}

sub topic_matches {
  my $sub_s = shift;
  my $topic_s = shift;

  my $ret = 0;

  return !!0 unless $sub_s && $topic_s;
  return !!1 if $sub_s eq $topic_s;

  my $sublen = length $sub_s;
  my $topiclen = length $topic_s;

  my @sub = split '', $sub_s;
  my @topic = split '', $topic_s;

  if (($sub[0] eq '$' && $topic[0] ne '$') || ($topic[0] eq '$' && $sub[0] ne '$')) {
    return !!0;
  }

  my ($spos, $tpos) = (0, 0);

  while($spos < $sublen && $tpos < $topiclen) {
    if ($sub[$spos] eq $topic[$tpos]) {
      if ($tpos == $topiclen-1) {
        # check for e.g. foo matching foo/#
        return !!1 if ($spos == $sublen-3) && ($sub[$spos+1] eq '/') && ($sub[$spos+2] eq '#');
      }

      $spos++;
      $tpos++;

      if($spos == $sublen && $tpos == $topiclen) {
        return !!1;
      } elsif ($tpos == $topiclen && $spos == $sublen-1 && $sub[$spos] eq '+') {
        return !!0 if $spos > 0 && $sub[$spos-1] eq '/';

        $spos++;
        return !!1;
      }
    } else {
      if ($sub[$spos] eq '+') {
        # check for bad "+foo" or "a/+foo" subscription
        return !!0 if ($spos > 0) && ($sub[$spos-1] ne '/');

        # check for bad "foo+" or "foo+/a" subscription */
        return !!0 if ($spos < $sublen-1) && ($sub[$spos+1] ne '/');

        $spos++;

        while($tpos < $topiclen && $topic[$tpos] ne '/') {
          $tpos++;
        }

        return !!1 if ($tpos == $topiclen) && ($spos == $sublen);
      } elsif ($sub[$spos] eq '#') {
        return !!0 if ($spos > 0) && ($sub[$spos-1] ne '/');

        return ($spos+1 == $sublen);
      } else {
        # check for e.g. foo/bar matching foo/+/#
        if(
          $spos > 0 &&
          $spos+2 == $sublen && $tpos == $topiclen &&
          $sub[$spos-1] eq '+' && $sub[$spos] eq '/' && $sub[$spos+1] eq '#'
        )
        {
          return !!1;
        }

        return !!0;
      }
    }
  }

  return !!0;
}

1;
