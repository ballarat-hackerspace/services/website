#
# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

package Plugins::Badges;
use Mojo::Base 'Mojolicious::Plugin';

use Mojo::JSON qw(decode_json encode_json);
use Mojo::SQLite;
use Mojo::Util qw(dumper trim);

use Carp 'croak';
use List::Util 'min';
use Time::Piece;

our $VERSION = '0.1';

has 'sqlite';
has 'uuid';

sub register {
  my ($self, $app, $config) = @_;

  $config->{db} //= ':temp:';

  $self->sqlite(state $sqlite = Mojo::SQLite->new->from_filename($config->{db}));

  $app->log->info("Using badges database: " . $config->{db});

  # conditionally build table
  $self->sqlite->db->query("
    CREATE TABLE IF NOT EXISTS badges (
      id        INTEGER PRIMARY KEY AUTOINCREMENT,
      title     CHAR(128) NOT NULL DEFAULT '',
      image_url TEXT NOT NULL DEFAULT '',
      criteria  TEXT NOT NULL DEFAULT '',
      category  CHAR(16) NOT NULL DEFAULT '',
      lifetime  INTEGER DEFAULT 0,
      meta      TEXT NOT NULL DEFAULT '{}',
      created   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      updated   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    );
  ");

  $app->helper('badges.assign' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    if ($args->{badge_id} && $args->{email}){

      my $badge_id = $args->{badge_id};
      my $email = trim ($args->{email} // '');

      return !!$self->sqlite->db->query('INSERT INTO members_badges (badge_id, email) VALUES (?, ?)',
          $badge_id, $email
      );
    }
    return undef;
  });

  $app->helper('badges.add' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $title = trim ($args->{title} // '');

    return undef unless !!$title;

    my $image_url = trim ($args->{image_url} // '');
    my $category = trim($args->{category} // '');
    my $criteria = trim($args->{criteria} // '');
    my $lifetime = $args->{lifetime};

    my $meta = encode_json($args->{meta} // {});

    return !!$self->sqlite->db->query('INSERT INTO badges (title, image_url, criteria, category, lifetime, meta) VALUES (?, ?, ?, ?, ?, ?)',
      $title, $image_url, $criteria, $category, $lifetime, $meta
    );
  });

  $app->helper('badges.find' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $fetch = $args->{fetch} // 10;
    my $filter = [];
    my $filter_arg = [];

    if ($args->{title}) {
      push @{$filter}, 'title=?';
      push @{$filter_arg}, $args->{title};
    }

    if ($args->{category}) {
      push @{$filter}, 'category=?';
      push @{$filter_arg}, $args->{category};
    }

    if ($args->{relative}) {
      my ($r_ts, $r_id) = split /\./, $args->{relative};
      push @{$filter}, '(timestamp, id) < (?, ?)';
      push @{$filter_arg}, $r_ts+0, $r_id+0;
    }

    my $where = @{$filter} ? ' WHERE ' . join(' AND ', @{$filter}) : '';
    my $limit = " ORDER BY created DESC, id DESC LIMIT $fetch";
    my $sql = "SELECT *, STRFTIME('%s', created) AS timestamp FROM badges $where $limit";

    my $badges = $self->sqlite->db->query($sql, @{$filter_arg})->hashes->each(sub {
      my $b = shift;

      $b->{meta} = decode_json $b->{meta};
    });

    my $data = {
      items => $badges->to_array,
    };

    my $last = $badges->last;
    $data->{relative} = "$last->{timestamp}.$last->{id}" if $last;

    return $data;
  });
  $app->helper('badges.delete' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $filter = [];
    my $filter_arg = [];

    if ($args->{id}) {
      push @{$filter}, 'id=?';
      push @{$filter_arg}, $args->{id};
    }

    if ($args->{title}) {
      push @{$filter}, 'title=?';
      push @{$filter_arg}, $args->{title};
    }

    return undef unless @{$filter};

    my $where = ' WHERE ' . join(' AND ', @{$filter});

    return !!$self->sqlite->db->query("DELETE FROM badges $where", @{$filter_arg});
  });

  $app->helper('badges.update' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $id = $args->{id} // '';
    my $title = trim ($args->{title} // '');

    return undef unless !!$id && !!$title;

    my $image_url = trim($args->{image_url} // '');
    my $category = trim($args->{category} // '');
    my $criteria = trim($args->{criteria} // '');
    my $lifetime = $args->{lifetime};

    my $meta = encode_json($args->{meta} // {});

    return !!$self->sqlite->db->query('UPDATE badges SET title=?, image_url=?, criteria=?, category=?, lifetime=?, meta=? WHERE id=?',
      $title, $image_url, $criteria, $category, $lifetime, $meta, $id
    );
  });

}

1;
