#
# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

package Plugins::Calendar;

use Mojo::Base 'Mojolicious::Plugin';
use feature 'postderef';

use Carp 'croak';
use Mojo::EventEmitter;
use Mojo::IOLoop;
use Mojo::URL;
use Mojo::UserAgent;
use Mojo::Util qw(dumper unquote url_escape);
use POSIX 'strftime';
use Time::Piece;

our $VERSION = '0.1';

has 'api_key';
has 'update_interval';
has 'url';

my $notifier = Mojo::EventEmitter->new;
my $ua = Mojo::UserAgent->new;

my $events = [];
my $events_moments = [];
my $moment_timers = [];

sub register {
  my ($plugin, $app, $config) = @_;

  my $id = $config->{id} // '';
  my $api_key = $config->{api_key} // 'bad';

  my $url = "https://www.googleapis.com/calendar/v3/calendars/$id/";
  my $update_interval = $config->{update_interval} // 10 * 60;

  $plugin->url($url);
  $plugin->api_key($api_key);
  $plugin->update_interval($update_interval);

  $app->log->info("Collecting Google calendar events every $update_interval seconds from: $id");

  $plugin->_update_gcal_events;

  Mojo::IOLoop->recurring($update_interval, sub {
    $app->log->info("Collecting Google calendar events from: $id");
    $plugin->_update_gcal_events;
  });

  $app->helper('events.list' => sub {
    my $c = shift;
    my $limit = shift // 10;

    # return empty for poor limit ranges
    return [] if $limit < 1;

    # return all if limit is higher than our count
    return $events if ($limit > @{$events});

    return [$events->@[0..$limit-1]];
  });

  $app->helper('events.list_by_day' => sub {
    my $c = shift;
    my $limit = shift // 10;

    # return empty for poor limit ranges
    return [] if $limit < 1 || @{$events} == 0;

    my $events_by_day = [];
    my $day_events = {};
    my $doy_last = -1;
    for my $event ($events->@[0..$limit-1]) {
      my $doy = $event->{start_time}->day_of_year;
      if ($doy != $doy_last) {
        push $events_by_day->@*, $day_events if exists $day_events->{events};

        $doy_last = $doy;
        $day_events = {
          start_time => $event->{start_time},
          events => [$event]
        }
      } else {
        push $day_events->{events}->@*, $event;
      }
    };

    # return all if limit is higher than our count
    return $events_by_day;
  });

  $app->helper('events.notifier' => sub {
    return $notifier;
  });
}

sub _update_gcal_events {
  my $self = shift;

  my $time_min = strftime("%FT00:00:00+1000", localtime);
  my $url = $self->url . sprintf("events/?maxResults=50&singleEvents=true&orderBy=startTime&timeMin=%s&key=%s", url_escape($time_min), $self->api_key);

  $ua->get($url => sub {
    my ($ua, $tx) = @_;
    my ($data, $err) = _process_response($tx);

    return if $err;

    $events = [];
    my $now = localtime;

    foreach my $item (@{$data->{items} // []}) {
      $item->{start}{dateTime} =~ s/([+-]\d\d):(\d\d)/$1$2/;
      $item->{end}{dateTime} =~ s/([+-]\d\d):(\d\d)/$1$2/;

      my $start_time = localtime(Time::Piece->strptime($item->{start}{dateTime}, "%Y-%m-%dT%H:%M:%S%z")->epoch);
      my $end_time = localtime(Time::Piece->strptime($item->{end}{dateTime}, "%Y-%m-%dT%H:%M:%S%z")->epoch);

      my $tags = [ grep { $_ } split(/\n?#/, $item->{description} // '') ];

      push @{$events}, {
        end_time    => $end_time,
        in_progress => ($start_time <= $now) && ($now <= $end_time),
        is_complete => ($now > $end_time),
        link        => $item->{htmlLink},
        location    => $item->{location},
        now         => $now,
        start_time  => $start_time,
        tags        => $tags,
        title       => $item->{summary},
      };
    }

    my $moments = [];

    for my $event ($events->@*) {
      push $moments->@*, {
        type => 'start',
        event => $event,
        distance => ($event->{start_time} - $now)->seconds
      };

      push $moments->@*, {
        type => 'end',
        event => $event,
        distance => ($event->{end_time} - $now)->seconds
      };
    }

    $events_moments = [ sort { $a->{distance} <=> $b->{distance} } $moments->@* ];

    # cancel existing timers
    Mojo::IOLoop->remove($_) for $moment_timers->@*;

    for my $moment (grep { $_->{distance} < $self->update_interval } $events_moments->@*) {
      if ($moment->{distance} > 0) {
        push $moment_timers->@*, Mojo::IOLoop->timer($moment->{distance}, sub {
          $notifier->emit($moment->{type} => $moment->{event});
        });
      }
    }
  });
}

sub _process_response {
  my $tx = shift;
  my ($data, $err);

  if ($err = $tx->error) {
    $err = $err->{message} || $err->{code};
  }
  elsif ($tx->res->headers->content_type =~ m!^(application/json|text/javascript)(;\s*charset=\S+)?$!) {
    $data = $tx->res->json;
  }

  # no data is an error of itself
  $err = $data ? '' : $err || 'Unknown error';

  return ($data, $err);
}

1;
