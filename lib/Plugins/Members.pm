#
# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

package Plugins::Members;
use Mojo::Base 'Mojolicious::Plugin';

#
# This plugin depends on plugins ::Badges, ::Devices and ::TidyHQ
#

use Carp 'croak';
use Mojo::JSON qw(decode_json encode_json);
use Mojo::SQLite;
use Mojo::Util 'dumper';
use Time::Piece;

our $VERSION = '0.1';

our $ROLES = {
  admin => 0x80000000,
  user  => 0x00000001
};

our $MEMBERSHIP_PREFERENCE_SCORE = {
  'association member' => 10,
  'social member' => 20,
  'yearly casual member' => 40,
  'full time member - hardship level' => 48,
  'full time member - volunteer level' => 49,
  'full time member' => 50,
  'benefactor - company/corporate membership' => 100,
  'philanthropist - company/corporate membership' => 200,
};

has 'sqlite';
has 'uuid';

sub register {
  my ($self, $app, $config) = @_;

  $config->{db} //= ':temp:';

  $self->sqlite(state $sqlite = Mojo::SQLite->new->from_filename($config->{db}));

  $app->log->info("Using members database: " . $config->{db});

  # conditionally build table
  $self->sqlite->db->query("
    CREATE TABLE IF NOT EXISTS members (
      email              CHAR(128) NOT NULL PRIMARY KEY,
      name               CHAR(128),
      avatar_url         CHAR(128),

      roles              INTEGER DEFAULT $ROLES->{user},

      membership         CHAR(32),
      membership_active  INTEGER DEFAULT 0,
      membership_expires TIMESTAMP,

      waiver_sign        INTEGER DEFAULT 0,
      waiver_signed      TIMESTAMP,

      data               TEXT NOT NULL DEFAULT '{}',
      meta               TEXT NOT NULL DEFAULT '{}',
      created            TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      updated            TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    )
  ");

  $self->sqlite->db->query("
    CREATE TABLE IF NOT EXISTS members_badges (
      email    CHAR(128) NOT NULL,
      badge_id INTEGER NOT NULL,
      granted  TIMESTAMP NOT NULL,
      expires  TIMESTAMP
    )
  ");

  $app->helper('member.create' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $email = $args->{email};

    unless ($email eq '' || length($email) > 128 || $email !~ m/[^@]+@[^\.]+\..+}/) {
      return undef;
    }

    my $now = gmtime;

    my $name = $args->{name};
    my $avatar_url = $args->{avatar_url} // "https://robohash.org/$email.png?size=200x200";

    my $roles = $args->{roles} // $ROLES->{user};

    my $membership = $args->{membership};
    my $membership_active = $args->{membership_active};
    my $membership_expires = $args->{membership_expires}->strftime('%F %T');

    my $waiver_sign = $args->{waiver_sign} // 0;
    my $waiver_signed = $waiver_sign ? $now->strftime('%F %T') : undef;

    # coercions
    $args->{meta}{last_login} = $args->{meta}{last_login}->strftime('%F %T') if $args->{meta}{last_login};

    my $data = encode_json($args->{data} // {});
    my $meta = encode_json($args->{meta} // {});

    return !!$self->sqlite->db->query(
      'INSERT INTO members (name, email, avatar_url, roles, membership, membership_active, membership_expires, waiver_sign, waiver_signed, data, meta) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
      $name, $email, $avatar_url, $roles, $membership, $membership_active,
      $membership_expires, $waiver_sign, $waiver_signed, $data, $meta
    );
  });

  $app->helper('member.find' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $member;

    # find by email
    if ($args->{email}) {
      $member = $self->sqlite->db->query('SELECT * FROM members WHERE email=?', $args->{email})->hashes->first;
    # find by device
    } elsif ($args->{device}) {
      $member = $self->sqlite->db->query('SELECT * FROM members m JOIN devices d ON (d.email=m.email) WHERE d.uuid=? AND d.token=?', $args->{device}{uuid}, $args->{device}{token})->hashes->first;
    }

    return undef unless $member;

    $member->{data} = decode_json $member->{data};
    $member->{meta} = decode_json $member->{meta};

    # coercions
    $member->{created} = Time::Piece->strptime($member->{created}, '%Y-%m-%d %H:%M:%S');
    $member->{updated} = Time::Piece->strptime($member->{updated}, '%Y-%m-%d %H:%M:%S');
    $member->{membership_expires} = Time::Piece->strptime($member->{membership_expires}, '%Y-%m-%d %H:%M:%S') if $member->{membership_expires};
    $member->{waiver_signed} = Time::Piece->strptime($member->{waiver_signed}, '%Y-%m-%d %H:%M:%S') if $member->{waiver_signed};
    $member->{meta}{last_login} = Time::Piece->strptime($member->{meta}{last_login}, '%Y-%m-%d %H:%M:%S') if $member->{meta}{last_login};

    return $member;
  });

  $app->helper('members.list' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $members = $self->sqlite->db->query('SELECT * FROM members')->hashes->each(sub {
      my $m = shift;

      $m->{data} = decode_json $m->{data};
      $m->{meta} = decode_json $m->{meta};

      # coercions
      $m->{created} = Time::Piece->strptime($m->{created}, '%Y-%m-%d %H:%M:%S');
      $m->{updated} = Time::Piece->strptime($m->{updated}, '%Y-%m-%d %H:%M:%S');
      $m->{membership_expires} = Time::Piece->strptime($m->{membership_expires}, '%Y-%m-%d %H:%M:%S') if $m->{membership_expires};
      $m->{waiver_signed} = Time::Piece->strptime($m->{waiver_signed}, '%Y-%m-%d %H:%M:%S') if $m->{waiver_signed};
      $m->{meta}{last_login} = Time::Piece->strptime($m->{meta}{last_login}, '%Y-%m-%d %H:%M:%S') if $m->{meta}{last_login};
    });

    return $members;
  });

  $app->helper('member.update' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $email = $args->{email};

    unless ($email eq '' || length($email) > 128 || $email !~ m/[^@]+@[^\.]+\..+}/) {
      return undef;
    }

    my $now = gmtime;

    my $name = $args->{name};
    my $avatar_url = $args->{avatar_url} // "https://robohash.org/$email.png?size=200x200";

    my $roles = $args->{roles} // $ROLES->{user};

    my $membership = $args->{membership};
    my $membership_active = $args->{membership_active};
    my $membership_expires = $args->{membership_expires}->strftime('%F %T');

    my $waiver_sign = $args->{waiver_sign} // 0;
    my $waiver_signed = $waiver_sign ? $now->strftime('%F %T') : undef;

    # coercions
    $args->{meta}{last_login} = $args->{meta}{last_login}->strftime('%F %T') if $args->{meta}{last_login};


    my $data = encode_json($args->{data} // {});
    my $meta = encode_json($args->{meta} // {});

    return !!$self->sqlite->db->query(
      'UPDATE members SET name=?, avatar_url=?, roles=?, membership=?, membership_active=?, membership_expires=?, waiver_sign=?, waiver_signed=?, data=?, meta=?, updated=CURRENT_TIMESTAMP WHERE email=?',
      $name, $avatar_url, $roles, $membership, $membership_active,
      $membership_expires, $waiver_sign, $waiver_signed, $data, $meta, $email
    );
  });

  $app->helper('member.login' => sub {
    my ($c, $email, $password) = @_;

    my $promise = $c->tidyhq->login(email => $email, password => $password)->then(sub {
      my $tidyhq = shift;

      my $member = $c->member->find(email => $email);

      my $now = gmtime;

      unless ($member) {

        my $member_create = {
          email => $email,
          name => sprintf('%s %s', $tidyhq->{contact}{first_name}, $tidyhq->{contact}{last_name}),
          avatar_url => $tidyhq->{contact}{profile_image},
          meta => {
            tidyhq => $tidyhq,
            logins => 1,
            last_login => $now,
            vetted => !!0,
          }
        };

        if (@{$tidyhq->{memberships}}) {
          # select the preferred membership
          my $membership = _memberships_preferred($tidyhq->{memberships});

          $member_create->{membership} = $membership->{name};
          $member_create->{membership_active} = $membership->{status};
          $member_create->{membership_expires} = $membership->{end_date};
        }

        $c->member->create($member_create);

        my $member = $c->member->find(email => $email);

        return Mojo::Promise->new->reject('Unable to create new membership.') unless ($member);
      } else {
        if (@{$tidyhq->{memberships}}) {
          # select the preferred membership
          my $membership = _memberships_preferred($tidyhq->{memberships});

          $member->{membership} = $membership->{name};
          $member->{membership_active} = $membership->{status};
          $member->{membership_expires} = $membership->{end_date};
        } else {
          $member->{membership} = undef;
          $member->{membership_active} = 0;
          $member->{membership_expires} = undef;
        }

        # update membership details
        $member->{meta}{tidyhq} = $tidyhq;
        $member->{meta}{logins} += 1;
        $member->{meta}{last_login} = $now;

        $c->member->update($member);
      }

      $c->session(user => $email);

      return Mojo::Promise->new->resolve($member);
    });
  });

  $app->helper('member.logout' => sub {
    my $c = shift;

    # clear our session member identifier
    $c->session(user => undef);

    # logout of any tidyhq sessions
    $c->tidyhq->logout;

    # logout of any pateron sessions
  });

  $app->helper('member.tidyhq.sync' => sub {
    my $c = shift;

    # force a refresh of all tidyhq users
    $c->tidyhq->sync;

    my $tidyhq_users = $c->tidyhq->users;

    for my $uid (keys %{$tidyhq_users}) {
      my $tidyhq = $tidyhq_users->{$uid};
      my $email = $tidyhq->{contact}{email_address};
      my $member = $c->member->find(email => $email);

      my $now = gmtime;

      if ($member) {
        if (@{$tidyhq->{memberships}}) {
          # select the preferred membership
          my $membership = _memberships_preferred($tidyhq->{memberships});

          $member->{membership} = $membership->{name};
          $member->{membership_active} = $membership->{status};
          $member->{membership_expires} = $membership->{end_date};
        } else {
          $member->{membership} = undef;
          $member->{membership_active} = 0;
          $member->{membership_expires} = undef;
        }

        # update membership details
        $member->{meta}{tidyhq} = {%{$member->{meta}{tidyhq}}, %{$tidyhq}};

        $c->member->update($member);
      } else {
        my $member_create = {
          email => $email,
          name => sprintf('%s %s', $tidyhq->{contact}{first_name}, $tidyhq->{contact}{last_name}),
          avatar_url => $tidyhq->{contact}{profile_image},
          meta => {
            tidyhq => $tidyhq,
            logins => 0,
            vetted => !!0,
          }
        };

        if (@{$tidyhq->{memberships}}) {
          my $membership = $tidyhq->{memberships}->[0];
          # use the first membership for now until we have tidyhq accounts with mulitple
          $member_create->{membership} = $membership->{name};
          $member_create->{membership_active} = $membership->{status};
          $member_create->{membership_expires} = $membership->{end_date};
        }

        $c->member->create($member_create);
      }
    }
  });

  $app->helper('member.current.profile' => sub {
    my $c = shift;
    my $user = $c->session('user') or return undef;

    my $member = $c->stash('member');
    $member = $c->member->find(email => $user) unless $member;

    return $member;
  });

  $app->helper('member.current.waiver_sign' => sub {
    my $c = shift;

    my $user = $c->session('user') or return undef;

    # fetch from stash if possible otherwise do a lookup
    my $member = $c->stash('member');
    $member = $c->member->find(email => $user) unless $member;

    $member->{waiver_sign} = 1;

    return $c->member->update($member);
  });

  $app->helper('member.has_membership' => sub {
    my ($c, $member) = (shift, shift);

    my $memberships = ref $_[0] eq 'ARRAY' ? shift : [ @_ ];

    return !!0 unless $member;

    return !!grep { $member->{membership} eq $_ } @{$memberships};
  });

  $app->helper('member.has_role' => sub {
    my ($c, $member, $role) = @_;

    return !!0 unless $member;

    return ($member->{roles} & $ROLES->{$role}) == $ROLES->{$role};
  });

  $app->helper('member.current.has_role' => sub {
    my ($c, $role) = @_;

    my $user = $c->session('user') or return !!0;

    # fetch from stash if possible otherwise do a lookup
    my $member = $c->stash('member');
    $member = $c->member->find(email => $user) unless $member;

    return !!0 unless $member;

    return ($member->{roles} & $ROLES->{$role}) == $ROLES->{$role};
  });

  $app->helper('member.current.has_active_membership' => sub {
    my $c = shift;
  });

  $app->helper('member.current.is_authenticated' => sub {
    my $c = shift;

    return $c->tidyhq->is_authenticated;
  });
}


sub _memberships_preferred {
  my $memberships = shift;

  my $sorted = [
    sort {
      $b->{status} <=> $a->{status} ||                                          # descending on status: activated, expired
      _membership_preference($b->{name}) <=> _membership_preference($a->{name}) # descending on score: higher score is preferred
    } @{$memberships}
  ];

  my $membership = $sorted->[0];

  return $membership;
}

sub _membership_preference {
  my $membership = shift;

  if (exists($MEMBERSHIP_PREFERENCE_SCORE->{$membership})) {
    return $MEMBERSHIP_PREFERENCE_SCORE->{$membership};
  }

  return 0;
};

1;
