#
# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

package Plugins::Devices;
use Mojo::Base 'Mojolicious::Plugin';

use Mojo::JSON qw(decode_json encode_json);
use Mojo::SQLite;
use Mojo::Util qw(dumper trim);

use Carp 'croak';
use DBI;
use Time::Piece;

our $VERSION = '0.1';

has 'sqlite';

sub register {
  my ($self, $app, $config) = @_;

  $config->{db} //= ':temp:';

  $self->sqlite(state $sqlite = Mojo::SQLite->new->from_filename($config->{db}));

  $app->log->info("Using device database: " . $config->{db});

  # conditionally build table
  $self->sqlite->db->query("
    CREATE TABLE IF NOT EXISTS devices (
      id        INTEGER PRIMARY KEY AUTOINCREMENT,
      email     CHAR(128) NOT NULL DEFAULT '',
      name      CHAR(128) NOT NULL DEFAULT '',
      uuid      CHAR(40) NOT NULL UNIQUE,
      token     CHAR(40) NOT NULL,

      roles     INTEGER NOT NULL DEFAULT 1,

      used      INTEGER NOT NULL DEFAULT 0,
      used_last TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

      meta      TEXT NOT NULL DEFAULT '{}',

      created   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      updated   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    )
  ");

  $app->helper('devices.exists' => sub {
    my $c = shift;
    my $uuid = shift;

    return $self->sqlite->db->query('SELECT * FROM devices WHERE uuid=?', $uuid)->hashes->size > 0;
  });

  $app->helper('devices.add' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $meta = encode_json($args->{meta} // {});

    my $ret;

    eval {
      $ret = !!$self->sqlite->db->query(
        'INSERT INTO devices (email, name, uuid, token, meta) VALUES (?, ?, ?, ?, ?)',
        $args->{email}, $args->{name}, $args->{uuid},
        $args->{token}, $meta
      );
    };

    if ($@) {
      $app->log->error("Unable to add device: $@");
      return !!0;
    }

    return $ret;
  });

  $app->helper('devices.find' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $fetch = $args->{fetch} // 1000;
    my $filter = [];
    my $filter_arg = [];

    if ($args->{name}) {
      push @{$filter}, 'name=?';
      push @{$filter_arg}, $args->{name};
    }

    if ($args->{uuid}) {
      push @{$filter}, 'uuid=?';
      push @{$filter_arg}, $args->{uuid};
    }

    if ($args->{email}) {
      push @{$filter}, 'email=?';
      push @{$filter_arg}, $args->{email};
    }

    if ($args->{relative}) {
      my ($r_ts, $r_id) = split /\./, $args->{relative};
      push @{$filter}, '(created_ts, id) > (?, ?)';
      push @{$filter_arg}, $r_ts+0, $r_id+0;
    }

    my $where = @{$filter} ? ' WHERE ' . join(' AND ', @{$filter}) : '';
    my $limit = " ORDER BY created, id LIMIT $fetch";

    #
    # note: we can't alias a virtual column as an already existing one
    # e.g STRFTIME('%s', created) AS created
    #
    my $sql = "SELECT id, name, email, uuid, token, roles, used, STRFTIME('%s', used_last) AS used_last_ts, meta, STRFTIME('%s', created) AS created_ts, STRFTIME('%s', updated) AS updated_ts FROM devices $where $limit";

    my $devices = $self->sqlite->db->query($sql, @{$filter_arg})->hashes->each(sub {
      my $d = shift;

      $d->{created} = delete $d->{created_ts};
      $d->{updated} = delete $d->{updated_ts};
      $d->{last_used} = delete $d->{last_used_ts};

      $d->{meta} = decode_json $d->{meta};
    });

    my $data = {
      items => $devices->to_array,
    };

    my $last = $devices->last;
    $data->{relative} = "$last->{created}.$last->{id}" if $last;

    return $data;
  });

  $app->helper('devices.delete' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $filter = [];
    my $filter_arg = [];

    if ($args->{uuid}) {
      push @{$filter}, 'uuid=?';
      push @{$filter_arg}, $args->{uuid};
    }

    if ($args->{email}) {
      push @{$filter}, 'email=?';
      push @{$filter_arg}, $args->{email};
    }

    return undef unless @{$filter};

    my $where = ' WHERE ' . join(' AND ', @{$filter});

    return !!$self->sqlite->db->query("DELETE FROM devices $where", @{$filter_arg});
  });

  $app->helper('devices.is_super' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    return !!0 unless $args->{uuid};

    return $self->sqlite->db->query('SELECT id FROM devices WHERE uuid=? AND (roles & 32768)', $args->{uuid})->hashes->size == 1;
  });

  $app->helper('devices.update' => sub {
    my $c = shift;
    my $args = @_%2 ? shift : {@_};

    my $uuid = $args->{uuid} // '';
    my $token = $args->{token} // '';
    my $email = trim($args->{email} // '');
    my $name = trim($args->{name} // '');

    return undef unless !!$uuid && !!$token && !!$email && !!$name;

    my $roles = $args->{roles} // 1;

    my $meta = encode_json($args->{meta} // {});

    return !!$self->sqlite->db->query('UPDATE devices SET email=?, name=?, roles=?, meta=? WHERE uuid=? AND token=?',
      $email, $name, $roles, $meta, $uuid, $token
    );
  });

  $app->helper('devices.validate' => sub {
    my $c = shift;

    my $uuid = shift;
    my $token = shift;

    return !!0 unless $uuid && $token;

    return $self->sqlite->db->query('UPDATE devices SET used=used+1, used_last=CURRENT_TIMESTAMP WHERE uuid=? AND token=?', $uuid, $token)->rows == 1;
  });
}

1;
