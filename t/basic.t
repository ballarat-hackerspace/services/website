# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

use FindBin;
use lib "$FindBin::Bin/lib";

use Test::More;
use Mojo::File 'path';

use Test::Mojo;

my $script = path(__FILE__)->dirname->sibling('bhackd');

$ENV{BHACKD_CONFIG} = 't/test.conf';

my $t = Test::Mojo->new($script);

$t->app->log->level('fatal');

# 'no backend code' endpoints
$t->get_ok('/about')->status_is(200);
$t->get_ok('/code-of-conduct')->status_is(200);
$t->get_ok('/contact')->status_is(200);
$t->get_ok('/faq')->status_is(200);
$t->get_ok('/security')->status_is(200);
$t->get_ok('/sponsors')->status_is(200);
$t->get_ok('/women')->status_is(200);
$t->get_ok('/workshops')->status_is(200);

# blog endpoints
$t->get_ok('/blog')->status_is(200);

# login/logout endpoints
$t->post_ok('/login')->status_is(400);

$t->get_ok('/logout')->status_is(302);
$t->post_ok('/logout')->status_is(302);

# model rules
$t->get_ok('/model-rules')->status_is(200);

# patreon redirect
$t->get_ok('/patreson')->status_is(302);

# meta - resources
$t->get_ok('/meta/resources')->status_is(200);

# meta - space api
$t->get_ok('/meta/space')->status_is(200);
$t->get_ok('/meta/space.json')->status_is(200);

# meta - webcams
$t->get_ok('/meta/webcams')->status_is(200);

# meta - aiart
$t->get_ok('/meta/aiart')->status_is(200);

# meta - streams
$t->get_ok('/meta/streams')->status_is(200);
$t->get_ok('/meta/streams/dashboard')->status_is(200);

$t->post_ok('/meta/streams')->status_is(400);
$t->post_ok('/meta/streams' => json => {stream => '@#$@'})->status_is(400);
$t->post_ok('/meta/streams' => json => {stream => 'foo', data => 'bar'})->status_is(200);

$t->get_ok('/meta/streams/foo')->status_is(200);
$t->get_ok('/meta/streams/foo.csv')->status_is(200);
$t->get_ok('/meta/streams/foo.json')->status_is(200);

$t->websocket_ok('/meta/streams-ws')->finish_ok;


# members grouping (not logged in)
$t->get_ok('/members')->status_is(302);
$t->get_ok('/members/profile/me')->status_is(302);
$t->get_ok('/members/profile/sign')->status_is(302);
$t->get_ok('/members/workshops')->status_is(302);


# admin grouping (not logged in)
$t->get_ok('/admin')->status_is(302);

$t->get_ok('/admin/badges')->status_is(302);
$t->post_ok('/admin/badges/add')->status_is(302);
$t->post_ok('/admin/badges/delete')->status_is(302);
$t->post_ok('/admin/badges/update')->status_is(302);

$t->get_ok('/admin/devices')->status_is(302);

$t->get_ok('/admin/streams')->status_is(302);
$t->post_ok('/admin/streams/delete')->status_is(302);

$t->get_ok('/admin/members')->status_is(302);
done_testing();
