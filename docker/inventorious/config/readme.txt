In here are the config files that are used by the front end.
The important one is REACT_APP_PARTS_URL, which is used on the front end to fetch from the backend API.
If you are self hosting this, you could set this to a relative path, otherwise set it to where you are hosting it.
Also if you are seeing this in the inventorious/config folder on your machine - congratulations it has copied across.