## Inventorious
For now this is the real root of the project. I plan to add a dashboard and real API hosting at the root location. 

Screens:
![Search](https://i.imgur.com/3sG7TG4.jpg), 
![View Details](https://i.imgur.com/OZI9uIZ.jpg),  
![Add](https://i.imgur.com/3sG7TG4.jpg) 

I would love for you to go and set this up - I am still in the stages of making this stage be as user friendly as possible.

Uses:
*  React
*  Components
*  JSON-Server for mocking and local hosting of API endpoints. It's super easy for a CRUD setup!

## Quick start

* clone this repo to your Linux or wsl box - I've been making it using WSL and love it!
* cd to search folder
* npm install        # I think you need to do this first time to fetch all the packages this application uses.
* check .env to see the local settings if you want to change them
* npm run "mock:api"       (Starts JSON-Server with your db.json file)
* in a new instance/tab/screen: npm start       (Starts the actual instance)
* browse http://localhost:4000/parts and verify it is showing the api end point being functional.
* browse http://localhost:3000 and use the search and add functions

## Further steps
You can set the environment values to point to your machines ip or hostname instead. If you do this, any machine can access this website on your network.



This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm run "mock:api"`
Use this to start up the JSON-Server instance. It's default location is set to be localhost:4000 , 
and is configured using environment variables in .env

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run "mock:api-hardcoded"`
If you are having trouble with your environment variables, you can edit this and run this instead to host your API.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
