import React from 'react'
import { Switch, Route, Router } from 'react-router-dom'
import AboutView from './About'
import SearchList from './SearchList'
import AddView from './Add'



function Main() {
    return (
      <main>
        <Switch>
          {/* <Router basename={'/parts'}> */}
            <Route exact path='/' component={SearchList}/>
            <Route path='/about' component={AboutView}/>
            <Route path='/add' component={AddView}/>
          {/* </Router> */}
        </Switch>
      </main>
    );
  }

  export default Main